﻿using EvoPdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SampleIronPdf.Controllers
{
    public class EvoPdfController : Controller
    {
        // GET: EvoPdf
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public FileResult HtmlContentToPdf(string html_content)
        {
            HtmlToPdfConverter htmlToPdfConverter = new HtmlToPdfConverter();
            htmlToPdfConverter.LicenseKey = "4W9+bn19bn5ue2B+bn1/YH98YHd3d3c=";
            htmlToPdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            htmlToPdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
            htmlToPdfConverter.PdfDocumentOptions.TopMargin = 1;
            htmlToPdfConverter.PdfDocumentOptions.BottomMargin = 1;
            htmlToPdfConverter.MediaType = "screen";
            htmlToPdfConverter.DefaultHtmlEncoding = "utf-8";

            string path_css = HttpContext.Server.MapPath("~/exportpdf/style.css");
            string text_css = System.IO.File.ReadAllText(path_css);
            html_content = $"<head>" +
                $"<meta charset=\"utf-8\" />" +
                $"<style type=\"text/css\">" +
                $"{text_css}" +
                $"</style>" +
                $"</head>" +
                $"<body>" +
                $"{html_content}" +
                $"</body>";
            html_content = $"<html>{html_content}</html>";

            var pdfDocument = htmlToPdfConverter.ConvertHtml(html_content, "");
            string file_name = "content.pdf";
            return File(pdfDocument, System.Net.Mime.MediaTypeNames.Application.Octet, file_name);
        }

    }
}