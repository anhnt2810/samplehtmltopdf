﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using IronPdf;

namespace SampleIronPdf.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public FileResult SamplePdf(string path_file)
        {
            HtmlToPdf htmlToPdf = new HtmlToPdf();
            htmlToPdf.PrintOptions.PaperSize = PdfPrintOptions.PdfPaperSize.A4;
            htmlToPdf.PrintOptions.PaperOrientation = PdfPrintOptions.PdfPaperOrientation.Portrait;
            htmlToPdf.PrintOptions.MarginTop = 1;
            htmlToPdf.PrintOptions.MarginBottom = 1;
            htmlToPdf.PrintOptions.InputEncoding = Encoding.UTF8;
            htmlToPdf.PrintOptions.PrintHtmlBackgrounds = true;
            htmlToPdf.PrintOptions.MarginLeft = 1;
            htmlToPdf.PrintOptions.MarginRight = 1;
            htmlToPdf.PrintOptions.CssMediaType = PdfPrintOptions.PdfCssMediaType.Print;

            PdfDocument pdfDocument;
            if (path_file.StartsWith("http"))
            {
                pdfDocument = htmlToPdf.RenderUrlAsPdf(path_file);
            }
            else
            {
                string path = HttpContext.Server.MapPath(path_file);
                pdfDocument = htmlToPdf.RenderHTMLFileAsPdf(path);
            }
            byte[] fileBytes = pdfDocument.BinaryData;
            string file_name = "content.pdf";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, file_name);
        }

        [HttpPost]
        [ValidateInput(false)]
        public FileResult HtmlContentToPdf(string html_content)
        {
            if(!html_content.Contains("<body>"))
            {
                html_content = $"<body>{html_content}</body>";
            }
            if(!html_content.StartsWith(@"<html>"))
            {
                html_content = $"<html>{html_content}</html>"; 
            }
            string div_page_name = "<div id=\"printView\">";
            string break_page = "<div style='page-break-after: always;'></div>";
            int start_check = 0;
            int index = html_content.IndexOf(div_page_name, start_check);
            start_check = index + div_page_name.Length;
            while (index > -1)
            {
                index = html_content.IndexOf(div_page_name, start_check);
                if(index > -1)
                {
                    html_content = html_content.Insert(index - 1, break_page);
                    start_check = index + break_page.Length + div_page_name.Length;
                }
            }
            HtmlToPdf htmlToPdf = new HtmlToPdf();
            htmlToPdf.PrintOptions.PaperSize = PdfPrintOptions.PdfPaperSize.A4;
            htmlToPdf.PrintOptions.PaperOrientation = PdfPrintOptions.PdfPaperOrientation.Portrait;
            htmlToPdf.PrintOptions.MarginTop = 1;
            htmlToPdf.PrintOptions.MarginBottom = 1;
            htmlToPdf.PrintOptions.InputEncoding = Encoding.UTF8;
            htmlToPdf.PrintOptions.CustomCssUrl = new Uri("http://localhost:65441/exportpdf/style.css");
            htmlToPdf.PrintOptions.CreatePdfFormsFromHtml = true;
            htmlToPdf.PrintOptions.PrintHtmlBackgrounds = true;
            htmlToPdf.PrintOptions.CssMediaType = PdfPrintOptions.PdfCssMediaType.Print;
            htmlToPdf.PrintOptions.RenderDelay = 1000;

            PdfDocument pdfDocument = htmlToPdf.RenderHtmlAsPdf(html_content);

            //if (path_file.StartsWith("http"))
            //{
            //    pdfDocument = htmlToPdf.RenderUrlAsPdf(path_file);
            //}
            //else
            //{
            //    string path = HttpContext.Server.MapPath(path_file);
            //    pdfDocument = htmlToPdf.RenderHTMLFileAsPdf(path);
            //}
            byte[] fileBytes = pdfDocument.BinaryData;
            string file_name = "content.pdf";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, file_name);
        }

    }
}